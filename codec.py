import codecs
import sys

text = u'pi: π'

# Wrap sys.stdout with a writer that knows how to handle encoding
# Unicode data.
wrapped_stdout = codecs.getwriter('UTF-8')(sys.stdout.buffer)
wrapped_stdout.write(text)

# Replace sys.stdout with a writer
sys.stdout = wrapped_stdout
