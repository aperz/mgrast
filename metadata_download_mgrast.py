import pandas as pd
import json
from urllib import request
import argparse
import os
import codecs
import sys


pd.set_option('display.width', 120)
pd.set_option('display.max_colwidth', 120)


######################
# I want this script to be independent
#from skrzynka.skrzynka import table_writer, insert_dummy_columns, decode_bytes
def decode_bytes(b, errors='ignore'):
    s = UnicodeDammit.detwingle(b)
    s = s.decode('utf-8', errors=errors).encode('utf-8').decode()
    return s

def insert_dummy_columns(df, colnames):
    '''
    colnames : list or pandas.Index
    '''
    missing_cols = set(colnames).difference(set(df.columns))
    for mc in missing_cols:
        df.insert(0, mc, "NaN")
    return df

def table_writer(o_fp, colnames, d = None, initiate=False):
    #TODO open file outside of function and handle as arg
    '''
    d : dict, pandas.core.frame.DataFrame (TODO: or list)
    initiate : bool; if True, only wrote the header. d is not used
    '''
    if initiate:
        # clean file and write header
        open(o_fp, 'w').close()
        header = "\t"+"\t".join([str(i) for i in colnames])+"\n"
        with open(o_fp, 'a') as o:
            o.write(header)
        return 'Header written to file'

    else:
        if isinstance(d, dict):
            dict2table_writer(o_fp, colnames, d)
        if isinstance(d, pd.core.frame.DataFrame):
            pandas2table_writer(o_fp, colnames, d)
        else:
            raise Exception('d is a', str(type(d)),'and is not a valid object')

###############


def get_list_projects(limit=""):
    #return pickle.load(open('project_ids.pkl', 'rb'))
    url = "http://api.metagenomics.anl.gov/project?limit={limit}"
    url = url.format(limit=limit)

    content = request.urlopen(url).read()
    d = pd.read_json(content)
    d = pd.DataFrame.from_dict(d['data'].to_dict(), orient='index')
    project_uris = d['url']
    project_ids = d['id']
    #pickle.dump(project_ids, open('project_ids.pkl', 'wb'))
    return project_ids


def get_project_md(pid):
    url_template = "http://api.metagenomics.anl.gov/metadata/export/{pid}"
    url = url_template.format(pid=pid)
    content = request.urlopen(url).read()
    #print(type(content))
    content = decode_bytes(content, errors='ignore')

    if len(content) == 0:
        return pd.DataFrame()

    m = pd.read_json(content, typ='series')
    #m = json.loads(content)


    ### SAMPLE IDS
    sample_ids = [i['id'] for i in m['samples']]

    if len(sample_ids) == 0:
        return pd.DataFrame()

    ### PROJECT DATA
    prj_data = pd.Series({k:v['value'] for k,v in m['data'].items()})
    prj_data = pd.concat([prj_data]*len(sample_ids), axis=1).T


    ###
    d = pd.Series(m['samples']).to_dict()
    d = pd.DataFrame(d).T

    ### ENVIRONMENT DATA
    #This sometimes has 'host_common_name', 'diet', 'disease_stat', 'sex', 'age' etc
    env_data = pd.DataFrame(d['envPackage'].to_dict()).T
    env_data = pd.DataFrame(env_data['data'].to_dict()).T
    env_data = env_data.applymap(lambda x: x['value'] if isinstance(x, dict) else "NaN") #THIS
    ###

    ### SAMPLE DATA
    sam_data = d['data'].to_dict()
    sam_data = [v for v in sam_data.values()]
    sam_data = pd.DataFrame.from_dict(sam_data)
    sam_data = sam_data.applymap(lambda x: x['value'] if isinstance(x, dict) else "NaN") #THIS

    if isinstance(prj_data, pd.core.frame.DataFrame):
        prj_data.index = sample_ids
        data = prj_data

        if isinstance(env_data, pd.core.frame.DataFrame):
            env_data.index = sample_ids
            data = pd.concat([data, env_data], axis=1)
        if isinstance(sam_data, pd.core.frame.DataFrame):
            sam_data.index = sample_ids
            data = pd.concat([data, sam_data], axis=1)

    #    print(env_data)
    #    print("-"*80)
    #    print(sam_data)
    #    print("-"*80)
    #    print(prj_data)
    #    print("="*80)

    return data


def get_metadata(odir):
    print("--- Getting the list of all projects...")
    prj_list = get_list_projects()
    print("--- There are " + str(len(prj_list)) + " projects.")
    #md = {}
    n_projects = 0
    print("--- Downloading metadata...")

    for pid in prj_list:
        n_projects =+1
        print("--- --- Downloading for", pid)

        data = get_project_md(pid)
        print(data.columns)
        data.insert(0, "project_id_from_download_script", pid)
        data.to_csv(str(odir)+"/mgrast_project_"+str(pid)+".tsv",sep='\t')
        #md[pid] = data

def integrate_files(odir):
    ofp = str(odir)+"/mgrast_all_metadata.tsv"

    print("--- Integrating information...")
    file_list = ['/D/MGRAST/metadata_MGRAST/mgrast_project_mgp10545.tsv', '/D/MGRAST/metadata_MGRAST/mgrast_project_mgp10381.tsv', '/D/MGRAST/metadata_MGRAST/mgrast_project_mgp65.tsv', '/D/MGRAST/metadata_MGRAST/mgrast_project_mgp5969.tsv', '/D/MGRAST/metadata_MGRAST/mgrast_project_mgp479.tsv', '/D/MGRAST/metadata_MGRAST/mgrast_project_mgp5355.tsv', '/D/MGRAST/metadata_MGRAST/mgrast_project_mgp15797.tsv', '/D/MGRAST/metadata_MGRAST/mgrast_project_mgp385.tsv', '/D/MGRAST/metadata_MGRAST/mgrast_project_mgp5885.tsv', '/D/MGRAST/metadata_MGRAST/mgrast_project_mgp5484.tsv', '/D/MGRAST/metadata_MGRAST/mgrast_project_mgp82113.tsv', '/D/MGRAST/metadata_MGRAST/mgrast_project_mgp7890.tsv']
    #file_list = [str(odir)+"/"+i for i in os.listdir(odir) if os.path.splitext(i)[-1] == '.tsv']
    all_colnames = []
    for f in file_list:
        #print(f)
        f_colnames = pd.read_csv(f, sep='\t', nrows=0, index_col=0).columns.tolist()
        all_colnames.extend(f_colnames)
    all_colnames = list(set(all_colnames))
    #return all_colnames


    table_writer(ofp, all_colnames, initiate=True)

    n_found = 0
    for f in file_list:
        table = pd.read_csv(f, sep='\t', index_col=0)
        if table.shape[0] > 0:
            n_found += table.shape[0]
            #print(f)
            #print("="*80)
            table_writer(ofp, all_colnames, d=table)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="A tool to retrieve project\
                        information and sample information\
                        from the MG-RAST database.\
                        ")
    parser.add_argument("-o", "--output_path",
                        help="The output directory.",
                        default = 'mgrast_metadata/',
                        required=False)
    args = vars(parser.parse_args())

    odir = args['output_path']

    if not os.path.exists(odir):
        os.mkdir(odir)

    # <3
    wrapped_stdout = codecs.getwriter('UTF-8')(sys.stdout.buffer)
    sys.stdout = wrapped_stdout
    #

    get_metadata(odir)
    integrate_files(odir)
    print("--- Program finished.")

